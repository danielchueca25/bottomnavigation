package com.daniel.bottonnavigation;

public class Pictures {
    private String imageUrl;
    private String description;
    private String namePic;

    public Pictures(String imageUrl, String description, String namePic) {
        this.imageUrl = imageUrl;
        this.description = description;
        this.namePic = namePic;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNamePic() {
        return namePic;
    }

    public void setNamePic(String namePic) {
        this.namePic = namePic;
    }
}
