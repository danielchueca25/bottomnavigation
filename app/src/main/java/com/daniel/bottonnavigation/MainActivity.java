package com.daniel.bottonnavigation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
private BottomNavigationView mMainNav;
private FrameLayout mMainFrame;
private HomeFragment homeFragment;
private ProfileFragment profileFragment;
private SettingsFragment settingsFragment;
private Toolbar toolbar;
private NavigationView navigationView;
private DrawerLayout drawerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainNav = (BottomNavigationView) findViewById(R.id.main_nav);
        mMainFrame = (FrameLayout) findViewById(R.id.main_frame);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        homeFragment = new HomeFragment();
        profileFragment = new ProfileFragment();
        settingsFragment = new SettingsFragment();
        homeFragment = HomeFragment.newInstance("Home1", "Home2");
        profileFragment = ProfileFragment.newInstance("Profile1","Profile2");
        settingsFragment =SettingsFragment.newInstance("Setting1","Setting2");
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                toolbar,
                R.string.open,
                R.string.close);
        drawerLayout.addDrawerListener(toggle);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        setFragment(homeFragment);
       mMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener(){
           @Override
           public boolean onNavigationItemSelected(@NonNull MenuItem item){
               switch (item.getItemId()){
                   case R.id.nav_home:
                       setFragment(homeFragment);
                       mMainNav.setItemBackgroundResource(R.color.design_default_color_primary_dark);
                       return true;
                   case R.id.nav_profile:
                       setFragment(profileFragment);
                       mMainNav.setItemBackgroundResource(R.color.design_default_color_primary);
                       return true;
                   case R.id.nav_settings:
                       setFragment(settingsFragment);
                       mMainNav.setItemBackgroundResource(R.color.design_default_color_secondary_variant);
                       return true;
                   default:
                       return false;
               }

           }
       });

    }
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_bike:
                Toast.makeText(this, "Bike menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_home:
                Toast.makeText(this, "Home menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_car:
                Toast.makeText(this, "Car menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_foot:
                Toast.makeText(this, "Foot menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_plane:
                Toast.makeText(this, "Plane menu", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_profile:
                Toast.makeText(this, "Profile ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_settings:
                Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_logout:
                Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
                break;
//add more
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }
}