package com.daniel.bottonnavigation;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    ArrayList<Pictures> pictures = new ArrayList<>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //make List,Grid,RecyclerView
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ImageView imageGrid = view.findViewById(R.id.imageGrid);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        initData();
        MyAdapter myAdapter = new MyAdapter(pictures,HomeFragment.this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3,RecyclerView.VERTICAL,false));


        return view;

    }


    private void initData() {
        Pictures pictures1 = new Pictures("https://joanseculi.com/images/img01.jpg", "Picture nº1", "Pic 01");
        pictures.add(pictures1);

        Pictures pictures2 = new Pictures("https://joanseculi.com/images/img02.jpg", "Picture nº2", "Pic 02");
        pictures.add(pictures2);

        Pictures pictures3 = new Pictures("https://joanseculi.com/images/img03.jpg", "Picture nº3", "Pic 03");
        pictures.add(pictures3);

        Pictures pictures4 = new Pictures("https://joanseculi.com/images/img04.jpg", "Picture nº4", "Pic 04");
        pictures.add(pictures4);

        Pictures pictures5 = new Pictures("https://joanseculi.com/images/img05.jpg", "Picture nº5", "Pic 05");
        pictures.add(pictures5);

        Pictures pictures6 = new Pictures("https://joanseculi.com/images/img06.jpg", "Picture nº6", "Pic 06");
        pictures.add(pictures6);

        Pictures pictures7 = new Pictures("https://joanseculi.com/images/img07.jpg", "Picture nº7", "Pic 07");
        pictures.add(pictures7);

        Pictures pictures8 = new Pictures("https://joanseculi.com/images/img08.jpg", "Picture nº8", "Pic 08");
        pictures.add(pictures8);

        Pictures pictures9 = new Pictures("https://joanseculi.com/images/img09.jpg", "Picture nº9", "Pic 09");
        pictures.add(pictures9);

        Pictures pictures10 = new Pictures("https://joanseculi.com/images/img10.jpg", "Picture nº10", "Pic 10");
        pictures.add(pictures10);

        Pictures pictures11 = new Pictures("https://joanseculi.com/images/img11.jpg", "Picture nº11", "Pic 11");
        pictures.add(pictures11);

        Pictures pictures12 = new Pictures("https://joanseculi.com/images/img12.jpg", "Picture nº12", "Pic 12");
        pictures.add(pictures12);

        Pictures pictures13 = new Pictures("https://joanseculi.com/images/img13.jpg", "Picture nº13", "Pic 13");
        pictures.add(pictures13);

        Pictures pictures14 = new Pictures("https://joanseculi.com/images/img14.jpg", "Picture nº14", "Pic 14");
        pictures.add(pictures14);

        Pictures pictures15 = new Pictures("https://joanseculi.com/images/img15.jpg", "Picture nº15", "Pic 15");
        pictures.add(pictures15);

        Pictures pictures16 = new Pictures("https://joanseculi.com/images/img16.jpg", "Picture nº16", "Pic 16");
        pictures.add(pictures16);

        Pictures pictures17 = new Pictures("https://joanseculi.com/images/img17.jpg", "Picture nº17", "Pic 17");
        pictures.add(pictures17);

        Pictures pictures18 = new Pictures("https://joanseculi.com/images/img18.jpg", "Picture nº18", "Pic 18");
        pictures.add(pictures18);

        Pictures pictures19 = new Pictures("https://joanseculi.com/images/img19.jpg", "Picture nº19", "Pic 19");
        pictures.add(pictures19);

        Pictures pictures20 = new Pictures("https://joanseculi.com/images/img20.jpg", "Picture nº20", "Pic 20");
        pictures.add(pictures20);

        Pictures pictures21 = new Pictures("https://joanseculi.com/images/img21.jpg", "Picture nº21", "Pic 21");
        pictures.add(pictures21);

        Pictures pictures22 = new Pictures("https://joanseculi.com/images/img22.jpg", "Picture nº22", "Pic 22");
        pictures.add(pictures22);

        Pictures pictures23 = new Pictures("https://joanseculi.com/images/img23.jpg", "Picture nº23", "Pic 23");
        pictures.add(pictures23);

        Pictures pictures24 = new Pictures("https://joanseculi.com/images/img24.jpg", "Picture nº24", "Pic 24");
        pictures.add(pictures24);

        Pictures pictures25 = new Pictures("https://joanseculi.com/images/img25.jpg", "Picture nº25", "Pic 25");
        pictures.add(pictures25);

        Pictures pictures26 = new Pictures("https://joanseculi.com/images/img26.jpg", "Picture nº26", "Pic 26");
        pictures.add(pictures26);

        Pictures pictures27 = new Pictures("https://joanseculi.com/images/img27.jpg", "Picture nº27", "Pic 27");
        pictures.add(pictures27);

        Pictures pictures28 = new Pictures("https://joanseculi.com/images/img28.jpg", "Picture nº28", "Pic 28");
        pictures.add(pictures28);

        Pictures pictures29= new Pictures("https://joanseculi.com/images/img29.jpg", "Picture nº29", "Pic 29");
        pictures.add(pictures29);

        Pictures pictures30 = new Pictures("https://joanseculi.com/images/img30.jpg", "Picture nº30", "Pic 30");
        pictures.add(pictures30);

        Pictures pictures31 = new Pictures("https://joanseculi.com/images/img31.jpg", "Picture nº31", "Pic 31");
        pictures.add(pictures31);

        Pictures pictures32 = new Pictures("https://joanseculi.com/images/img32.jpg", "Picture nº32", "Pic 32");
        pictures.add(pictures32);

        Pictures pictures33 = new Pictures("https://joanseculi.com/images/img33.jpg", "Picture nº33", "Pic 33");
        pictures.add(pictures33);

        Pictures pictures34 = new Pictures("https://joanseculi.com/images/img34.jpg", "Picture nº34", "Pic 34");
        pictures.add(pictures34);

        Pictures pictures35 = new Pictures("https://joanseculi.com/images/img35.jpg", "Picture nº35", "Pic 35");
        pictures.add(pictures35);

        Pictures pictures36 = new Pictures("https://joanseculi.com/images/img36.jpg", "Picture nº36", "Pic 36");
        pictures.add(pictures36);

        Pictures pictures37 = new Pictures("https://joanseculi.com/images/img37.jpg", "Picture nº37", "Pic 37");
        pictures.add(pictures37);

        Pictures pictures38 = new Pictures("https://joanseculi.com/images/img38.jpg", "Picture nº38", "Pic 38");
        pictures.add(pictures38);

    }


}