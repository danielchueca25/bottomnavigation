package com.daniel.bottonnavigation;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter <MyAdapter.MyViewHolder> {
    private Fragment context;
    private ArrayList<Pictures> pictures;
    private DetailsFragment detailsFragment;
    public MyAdapter(ArrayList<Pictures> pictures, Fragment context) {
        this.context = context;
        this.pictures = pictures;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        private ImageView imageGrid;
        private TextView textgrid;

        RelativeLayout grigData;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textgrid= itemView.findViewById(R.id.textGrid);
            imageGrid= itemView.findViewById(R.id.imageGrid);


            grigData = itemView.findViewById(R.id.gridData);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context.getContext());
        View view = inflater.inflate(R.layout.grid_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(pictures.get(position).getImageUrl())
                .fit()
                .centerCrop()
                .into(holder.imageGrid);

        holder.textgrid.setText(pictures.get(position).getNamePic());
        holder.imageGrid.setOnClickListener(v -> {
             detailsFragment= DetailsFragment.newInstance(pictures.get(holder.getAdapterPosition()).getNamePic(),
                    pictures.get(holder.getAdapterPosition()).getImageUrl(), pictures.get(holder.getAdapterPosition()).getDescription());
            FragmentManager fragmentManager= ((FragmentActivity)context.getContext()).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_frame,detailsFragment);
            fragmentTransaction.commit();
        });

    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }


    }

